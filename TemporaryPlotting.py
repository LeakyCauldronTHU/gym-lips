import matplotlib.pyplot as plt
import numpy as np
import math
import os

import matplotlib
SMALL_SIZE = 6
matplotlib.rc('font', size=SMALL_SIZE)
matplotlib.rc('axes', titlesize=SMALL_SIZE)


def str_process(name):
    name = name.split('=')
    name_ret = ''
    # print(name)
    for i in range(len(name)):
        name_ret += name[i][0:3]
        name_ret += '_'
    name_ret = name_ret[:-1]
    return name_ret


def plotting(files_dir, hypers=None, appID=None):
    print("stream check I")

    total_timesteps, eprewmean, policy_entropy, \
    policy_loss_at, policy_loss_pi, value_loss, nupdates = [], [], [], [], [], [], []

    logs = open(files_dir)
    line = logs.readline()

    values = []
    titles = []

    name = 'test'
    name_for_title = 'test'

    # print("name for title", name_for_title)

    while line:
        if 'log_path' in line and 'alg' not in line:
            tmp = line.split(' ')[-1]
            log = tmp.split('/')[-1]
        if 'total_timesteps' in line:
            a = line.split('|')
            total_timesteps.append(int(a[2]))

        if 'nupdates' in line:
            a = line.split('|')
            nupdates.append(int(a[2]))

        if 'eprewmean' in line:
            if 'eprewmean' not in titles:
                titles.append('eprewmean')
            a = line.split('|')
            eprewmean.append(float(a[2]))

        if 'policy_entropy' in line and 'unattacked' not in line:
            if 'policy_entropy' not in titles:
                titles.append('policy_entropy')
            a = line.split('|')
            policy_entropy.append(float(a[2]))

        if 'policy_loss_at' in line:
            if 'policy_loss_at' not in titles:
                titles.append('policy_loss_at')
            a = line.split('|')
            policy_loss_at.append(float(a[2]))

        if 'policy_loss_pi' in line:
            if 'policy_loss_pi' not in titles:
                titles.append('policy_loss_pi')
            a = line.split('|')
            policy_loss_pi.append(float(a[2]))

        #
        # if 'value_loss' in line:
        #     if 'value_loss' not in titles:
        #         titles.append('value_loss')
        #     a = line.split('|')
        #     value_loss.append(float(a[2]))

        line = logs.readline()
    print(total_timesteps)
    total_timesteps = total_timesteps[-1]
    nupdates = nupdates[-1]
    print("total_timesteps and nupdates", total_timesteps, nupdates)

    print("stream check II")

    for t in titles:
        values.append(eval(t))

    num_fig = len(titles)
    neglect_list = ['value_loss']
    plt.figure()
    plt.subplots_adjust(hspace=0.5, wspace=0.4)
    row = math.ceil(float(num_fig) / 3.0)
    if row <= 1:
        row = 2

    position = 0
    for i in range(len(titles)):
        if titles[i] in neglect_list:
            pass
        else:
            plt.subplot(row, 3, position + 1)
            index = total_timesteps // len(values[i]) * np.arange(len(values[i]))
            plt.plot(index, values[i], linewidth=0.5)
            plt.xlabel('training steps')
            plt.grid()
            plt.title(titles[i])
            position += 1

    plt.suptitle(name_for_title, ha='center', wrap=True)
    # plt.legend()
    # print("is saving figure", name)
    exist = os.listdir('d:/figures')
    if name + '.png' in exist:
        print("this figure already exist in this path, so we did not save it")
    else:
        plt.savefig('d:/figures/' + name, dpi=300)
    plt.show()
    plt.close()
    stats = dict(zip(titles, values))
    print("stream check III")

    try:
        plt.figure()
        plt.subplot(2, 3, 1)
        index = total_timesteps // len(stats['policy_entropy'][1::2]) * np.arange(len(stats['policy_entropy'][1::2]))
        plt.plot(index, stats['policy_entropy'][1::2], label='attacked', linewidth=0.5)
        index = total_timesteps // len(stats['policy_entropy_unattacked']) * np.arange(
            len(stats['policy_entropy_unattacked']))
        plt.plot(index, stats['policy_entropy_unattacked'], label='unattacked', linewidth=0.5)
        plt.xlabel('training steps')
        plt.title('policy_entropy unattacked_VS_attacked')
        plt.legend()
        plt.grid()
        # plt.show()
        plt.subplot(2, 3, 2)
        index = total_timesteps // len(stats['cross_entropy_loss_init']) * np.arange(
            len(stats['cross_entropy_loss_init']))
        plt.plot(index, stats['cross_entropy_loss_init'], label='initial', linewidth=0.5)
        plt.plot(index, stats['cross_entropy_loss_term'], label='terminal', linewidth=0.5)
        plt.xlabel('training steps')
        plt.title("cross_entropy_loss initial_VS_termimal")

        plt.suptitle(name_for_title + '_statistics', ha='center', wrap=True)
        plt.legend()
        plt.grid()

        exist = os.listdir('d:/figures')
        # print("exist", exist, 'd:/figures/' + name + '_statistics' + '.png')
        if name + '_statistics' + '.png' in exist:
            print("this figure already exist in this path, so we did not save it")
        else:
            plt.savefig('d:/figures/' + name + '_statistics', dpi=300)
        # plt.show()
        plt.close()
    except:
        pass



if __name__ == "__main__":
    file_dir = 'd:/log.txt'
    # file_dir = 'd:/stdout.txt'
    plotting(file_dir)