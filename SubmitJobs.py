# coding=utf-8
import os
import io
import subprocess
import json
import csv

from datetime import datetime

code_name = 'gym-lips'

# cluster = "gcr"
# cluster = "cam"

# cluster = "rr2"

cluster = 'rr1'

version = "start"

PHILLY_FS = r'"\\scratch2\scratch\Philly\philly-fs\windows\philly-fs.exe"'

vc = "resrchvc"
user = 'v-wancha'
passwd = ''
os.environ["PHILLY_VC"] = vc


def philly_cmd(commandline):
    subprocess.call(r'{0} {1}'.format(PHILLY_FS, commandline), shell=True)


workdir_philly_fs = r"gfs://{0}/{1}/{2}".format(cluster, vc, user)
input_dir = r"/hdfs/resrchvc/v-wancha"

local_config_folder = r"D:\Mahjong-Models-Pytorch-v{0}\script-bash\config".format(version)
remote_config_folder = r'{0}/models/configs/'.format(workdir_philly_fs)


# local_bash_folder = r"D:\shell" + code_name
local_bash_folder = os.path.join("c:\d\shell", code_name + "-" + cluster)
os.makedirs(local_bash_folder, exist_ok=True)

remote_bash_folder = r'{0}/script-bash/script'.format(workdir_philly_fs)
# remote_bash_linux_folder = r"/hdfs/{0}/{1}/{2}/script-bash/script".format(vc, user, proj)
remote_bash_linux_folder = (r"//philly/" + cluster + "/resrchvc/{0}/shell/" + code_name + "-" + cluster).format(user)
remote_bash_folder = remote_bash_linux_folder

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
# print("local config folder: ", local_config_folder)
# print("remote config folder: ", remote_config_folder)
# print("local bash folder: ", local_bash_folder)
print("remote bash folder: ", remote_bash_folder)
print("remtoe bash linux folder: ", remote_bash_linux_folder)
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print()


def upload_config(filename, submit=False):
    local_config = r'{0}\{1}'.format(local_config_folder, filename)
    print("\n~~~~~~~~~~~~~~~upload_config~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("local config file:", local_config)
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
    if submit:
        philly_cmd('-cp {0} {1}/'.format(local_config, remote_config_folder))
    return local_config


def upload_bash(filename, submit=False):
    local_bash = r'{0}\{1}'.format(local_bash_folder, filename) + '.sh'
    remote_bash = r'{0}'.format(remote_bash_folder)

    print("\n~~~~~~~~~~~~~~~upload_bash~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("local bash file: ", local_bash)
    print("remtoe bash file: ", remote_bash)
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")

    if submit:
        philly_cmd('-cp {0} {1}'.format(local_bash, remote_bash_folder))
        philly_cmd('-chmod 777 {0}'.format(remote_bash))
    print("philly command line", '-cp {0} {1}'.format(local_bash, remote_bash_folder))
    exit(0)
    return remote_bash


def submit_job(jobname, filename, submit=False):
    print("file name in submission", filename)
    remote_bash = r'{0}/{1}'.format(remote_bash_linux_folder, filename)
    print("remote bash", remote_bash)

    head_url = "https://philly/api/submit?"
    CMD = "clusterId={0}&".format(cluster)
    CMD += "registry=phillyregistry.azurecr.io&"
    CMD += "repository=philly/jobs/custom/tensorflow&tag=tf110-py36&"
    # CMD += "repository=philly/jobs/custom/tensorflow&tag=tf113-py36-nccl2&"
    CMD += "buildId=0000&clusterId={0}&".format(cluster)
    CMD += "vcId={0}&".format(vc)
    CMD += "rackid=anyConnected&"
    if cluster == 'cam':
        CMD += "Queue={0}&".format("bonus")
    CMD += "configFile={0}&".format(remote_bash)
    CMD += "minGPUs=1&name={0}&".format(jobname)
    CMD += "isdebug=False&ismemcheck=false&isperftrace=false&iscrossrack=false&oneProcessPerContainer=true&dynamicContainerSize=false&numOfContainers=1"
    CMD += "&inputDir={0}&userName={1}&submitCode=p".format(input_dir, user)

    url = head_url + CMD

    print("~~~~~~~~~~~~~~~~~~~~~submit job~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    # print("\n" + url + "\n")
    # print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

    max_submit_times = 20
    counter = 0
    if submit:
        while counter < max_submit_times:
            print("Is submitting the job for the {} time......".format(counter+1))
            counter += 1
            # os.system(r'curl -k --ntlm --user "{0}:{1}" "{2}"'.format(user, passwd, url))
            # response = os.popen(r'curl -k --ntlm --user "{0}:{1}" "{2}"'.format(user, passwd, url)).readlines()
            response = os.popen(r'curl -k --ntlm --user : "{0}"'.format(url)).readlines()

            content = json.loads(response[0])
            print("content: ", content)
            # if "error" in content:
            #     print("error detected")

            if "jobId" in content:
                print(content["jobId"][12:])
                return content["jobId"][12:]
            else:
                print("re-submit jobs")
                # return "-1"
        if "error" in content:
            print("fail to submit jobs")
            return "-1"
    print("\n~~~~~~~~~~~~~~~~~~finish submit~~~~~~~~~~~~~~~~~~~~~")


def save_bash(config_file_list, submit_type, file_name):
    bash_file = os.path.join(local_bash_folder, file_name + ".sh")
    if submit_type == 'attack':
        command = r"python -m baselines.run --alg=ppo2 --num_timesteps=1e8 --env={0} --seed={1} --training_type={2} " \
                  r"--lipschitz_distance={3} --coef_lips={4} --cluster={5}" \
            .format(config_file_list[0], config_file_list[1], config_file_list[2], config_file_list[3],
                    config_file_list[4], cluster)
    if submit_type == 'normal':
        command = r"python -m baselines.run --alg=ppo2 --num_timesteps=1e8 --env={0} --seed={1} --training_type={2} " \
                  r" --cluster={3}" \
            .format(config_file_list[0], config_file_list[1], config_file_list[2], cluster)

    if submit_type == 'test':
        # command = r'python -m baselines.run --alg=ppo2 --num_timesteps=1e8 --env=BoxingNoFrameskip-v4 --seed=0 ' \
        #           r'--training_type=attack --cluster=gcr --load_path=//philly/gcr/resrchvc/v-wancha/gym_lips_logs/02000 ' \
        #           r'--test'
        command = r'python -m baselines.run --alg=ppo2 --num_timesteps=1e8 --env=BoxingNoFrameskip-v4 --seed=0 ' \
                  r'--training_type=attack --cluster={} --lipschitz_distance=0.5 --coef_lips=5.0 --network_size=1 ' \
                  r'--test'.format(cluster, cluster)

    print("\n~~~~~~~~~~~~~~~ our new bash file ~~~~~~~~~~~~")
    print("bash file name", bash_file)
    print("commond line", command)
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

    with io.open(bash_file, "w", newline='\n') as f:

        f.write("pip install --user gym[atari]"+"\n")
        f.write("pip install --user msgpack"+"\n")
        f.write("pip install --user matplotlib"+"\n")
        f.write("cd ~"+"\n")
        f.write("git clone https://DennisWangWC@bitbucket.org/DennisWangWC/" + code_name + ".git"+"\n")
        f.write("cd " + code_name +"\n")
        f.write("cd baselines"+"\n")
        f.write("pip install --user -e ."+"\n")
        f.write("cd .."+"\n")
        f.write(command)


if __name__ == "__main__":

    # submit_type = 'attack'
    # submit_type = 'normal'
    submit_type = 'test'

    if submit_type == 'normal':
        # environment = ['CentipedeNoFrameskip-v4', 'ChopperCommandNoFrameskip-v4', 'AssaultNoFrameskip-v4',
        #                'AirRaidNoFrameskip-v4', 'AsterixNoFrameskip-v4', 'AtlantisNoFrameskip-v4',
        #                'FishingDerbyNoFrameskip-v4',
        #                'FreewayNoFrameskip-v4', 'IceHockeyNoFrameskip-v4', 'JourneyEscapeNoFrameskip-v4',
        #                'SkiingNoFrameskip-v4']

        environment = ['BoxingNoFrameskip-v4', 'BreakoutNoFrameskip-v4', 'SpaceInvadersNoFrameskip-v4']
        training_type = ['normal']
        network_size = ['1']
        seed = ['0']
        counter = 0
        for env in environment:
            for tra in training_type:
                for see in seed:
                    for net in network_size:
                        counter += 1
                        bash_filename = "env=" + env + "-see=" + see + "-tra=" + tra
                        bash_filename = bash_filename.replace('.', '_')
                        bash_filename += '.sh'
                        if counter > 0:
                            # print("bash_file", bash_filename)
                            save_bash([env, see, tra, net], submit_type, bash_filename.split('.')[0])
                            # jobname = code_name + "-" + bash_filename.split('.')[0]
                            # jobId = submit_job(jobname, bash_filename, True)
                            # exit(0)

    if submit_type == 'attack':
        # environment = ['BoxingNoFrameskip-v4', 'BreakoutNoFrameskip-v4']
        environment = ['SpaceInvadersNoFrameskip-v4', 'CarnivalNoFrameskip-v4']
        training_type = ['attack']
        seed = ['0']
        learning_rate_times = ['1.0']
        lipschitz_distance = ['0.2', '1.0', '5.0']
        coef_lips = ['0.2', '1.0', '5.0']
        counter = 0
        for env in environment:
            for tra in training_type:
                for see in seed:
                    for lip in lipschitz_distance:
                        for coe in coef_lips:
                            counter += 1
                            bash_filename = "env=" + env + "-see=" + see + "-tra=" + tra + "-lip=" + lip + "-coe=" + coe
                            bash_filename = bash_filename.replace('.', '_')
                            bash_filename += '.sh'
                            if counter > 0:
                                # print("bash_file", bash_filename)
                                # save_bash([env, see, tra, lip, coe], submit_type, bash_filename.split('.')[0])
                                jobname = code_name + "-" + bash_filename.split('.')[0]
                                jobId = submit_job(jobname, bash_filename, True)
                                # exit(0)

    if submit_type == 'test':
        bash_filename = "test"
        bash_filename = bash_filename.replace('.', '_')
        bash_filename += '.sh'
        # save_bash([], submit_type, bash_filename.split('.')[0])
        jobname = code_name + "-" + bash_filename.split('.')[0]
        jobId = submit_job(jobname, bash_filename, True)
        # exit(0)
