import tensorflow as tf
import functools

from baselines.common.tf_util import get_session, save_variables, load_variables
from baselines.common.tf_util import initialize

try:
    from baselines.common.mpi_adam_optimizer import MpiAdamOptimizer
    from mpi4py import MPI
    from baselines.common.mpi_util import sync_from_root
except ImportError:
    MPI = None

from baselines import logger
import numpy as np


class Model(object):
    """
    We use this object to :
    __init__:
    - Creates the step_model
    - Creates the train_model

    train():
    - Make the training part (feedforward and retropropagation of gradients)

    save/load():
    - Save load the model
    """
    def __init__(self, *, policy, ob_space, ac_space, nbatch_act, nbatch_train,
                nsteps, ent_coef, weight_coef, vf_coef, max_grad_norm, attack_args=None, mpi_rank_weight=1, comm=None, microbatch_size=None):
        self.sess = sess = get_session()
        self.attack_args=attack_args

        if MPI is not None and comm is None:
            comm = MPI.COMM_WORLD

        with tf.variable_scope('ppo2_model', reuse=tf.AUTO_REUSE):
            # CREATE OUR TWO MODELS
            # act_model that is used for sampling
            act_model = policy(nbatch_act, 1, sess)

            # Train model for training
            if microbatch_size is None:
                train_model = policy(nbatch_train, nsteps, sess)
            else:
                train_model = policy(microbatch_size, nsteps, sess)

        # CREATE THE PLACEHOLDERS
        self.A = A = train_model.pdtype.sample_placeholder([None])
        self.ADV = ADV = tf.placeholder(tf.float32, [None])
        self.R = R = tf.placeholder(tf.float32, [None])
        # Keep track of old actor
        self.OLDNEGLOGPAC = OLDNEGLOGPAC = tf.placeholder(tf.float32, [None])
        # Keep track of old critic
        self.OLDVPRED = OLDVPRED = tf.placeholder(tf.float32, [None])
        self.LR = LR = tf.placeholder(tf.float32, [])
        self.LR_FOR_ATTACK = LR_FOR_ATTACK = tf.placeholder(tf.float32, [])
        # Cliprange
        self.CLIPRANGE = CLIPRANGE = tf.placeholder(tf.float32, [])

        neglogpac = train_model.pd.neglogp(A)

        # Calculate the entropy
        # Entropy is used to improve exploration by limiting the premature convergence to suboptimal policy.
        entropy = tf.reduce_mean(train_model.pd.entropy())
        # lipschitz_distance = train_model.pd.kl(train_model.pd_attacked)

        # dist_normal = tf.exp(train_model.pd.logdist()) + 1e-10
        dist_normal = tf.exp(train_model.pd.logdist()) + 1e-5
        # dist_attacked = tf.exp(train_model.pd_attacked.logdist()) + 1e-10
        dist_attacked = tf.exp(train_model.pd_attacked.logdist()) + 1e-5
        lipschitz_distance = self.kl_div(dist_normal, dist_attacked)

        self.lips_max_min_mean = [tf.reduce_max(lipschitz_distance), tf.reduce_min(lipschitz_distance),
                                  tf.reduce_mean(lipschitz_distance)]

        lipschitz_loss = tf.reduce_mean(tf.maximum(lipschitz_distance - attack_args.lipschitz_distance * 0.03, 0))

        # value loss
        vpred = train_model.vf
        vpredclipped = OLDVPRED + tf.clip_by_value(train_model.vf - OLDVPRED, - CLIPRANGE, CLIPRANGE)
        vf_losses1 = tf.square(vpred - R)
        vf_losses2 = tf.square(vpredclipped - R)
        vf_loss = .5 * tf.reduce_mean(tf.maximum(vf_losses1, vf_losses2))

        # policy loss (for adversarial and for normal)
        weights = sum([tf.nn.l2_loss(params) for params in tf.trainable_variables('ppo2_model/pi')])
        ratio = tf.exp(OLDNEGLOGPAC - neglogpac)
        pg_losses_normal_1 = -ADV * ratio
        pg_losses_normal_2 = -ADV * tf.clip_by_value(ratio, 1.0 - CLIPRANGE, 1.0 + CLIPRANGE)
        pg_loss_normal = tf.reduce_mean(tf.maximum(pg_losses_normal_1, pg_losses_normal_2)) \
                         - entropy * ent_coef + weights * weight_coef
        pg_loss_lips_regu = pg_loss_normal + attack_args.coef_lips * lipschitz_loss

        # the statistical evaluation of the kl divergence between \pi_old and \pi
        approxkl = .5 * tf.reduce_mean(tf.square(neglogpac - OLDNEGLOGPAC))
        clipfrac = tf.reduce_mean(tf.to_float(tf.greater(tf.abs(ratio - 1.0), CLIPRANGE)))

        # UPDATE THE PARAMETERS USING LOSS
        # 1. Get the model parameters
        params_pi = tf.trainable_variables('ppo2_model/pi')
        params_vf = tf.trainable_variables('ppo2_model/vf')
        # print("params_at", params_at)
        # 2. Build our trainer
        if comm is not None and comm.Get_size() > 1:
            self.trainer_pi = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
            # self.trainer_pi_lips_regu = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
            self.trainer_pi_lips_regu = self.trainer_pi
            self.trainer_vf = MpiAdamOptimizer(comm, learning_rate=LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)
        else:
            self.trainer_pi = tf.train.AdamOptimizer(learning_rate=LR, epsilon=1e-5)
            # self.trainer_pi_lips_regu = tf.train.AdamOptimizer(learning_rate=LR, epsilon=1e-5)
            self.trainer_pi_lips_regu = self.trainer_pi
            self.trainer_vf = tf.train.AdamOptimizer(learning_rate=LR, epsilon=1e-5)
        # 3. Calculate the gradients
        # grads_and_var_pi = self.trainer_pi.compute_gradients(pg_loss_normal, params_pi)
        grads_and_var_pi = self.trainer_pi_lips_regu.compute_gradients(pg_loss_normal, params_pi)
        grads_and_var_pi_lips_regu = self.trainer_pi_lips_regu.compute_gradients(pg_loss_lips_regu, params_pi)
        grads_and_var_vf = self.trainer_vf.compute_gradients(vf_loss, params_vf)
        grads_and_var_pi_lips = self.trainer_pi_lips_regu.compute_gradients(lipschitz_loss, params_pi)
        grads_and_var_ent = self.trainer_pi_lips_regu.compute_gradients(entropy, params_pi)
        grads_and_var_pi_lips_original = self.trainer_pi_lips_regu.compute_gradients(lipschitz_distance, params_pi)

        grads_pi, var_pi = zip(*grads_and_var_pi)
        grads_pi_lips_regu, var_pi_lips_regu = zip(*grads_and_var_pi_lips_regu)
        grads_vf, var_vf = zip(*grads_and_var_vf)
        # grads_pi_lips, var_pi_lips = zip(*grads_and_var_pi_lips)

        self.grads_pi_lips_regu_norm = [tf.norm(grads_and_var_pi_lips_regu[i]) for i in range(len(grads_and_var_pi_lips_regu))]
        self.grads_pi_lips_norm = [tf.norm(grads_and_var_pi_lips[i]) for i in range(len(grads_and_var_pi_lips))]
        self.grads_pi_norm = [tf.norm(grads_and_var_pi[i]) for i in range(len(grads_and_var_pi))]
        self.pi_norm = [tf.norm(params_pi[i]) for i in range(len(params_pi))]
        self.grads_ent_norm = [tf.norm(grads_and_var_ent[i]) for i in range(len(grads_and_var_ent))]
        self.grads_pi_lips_original_norm = [tf.norm(grads_and_var_pi_lips_original[i]) for i in range(len(grads_and_var_pi_lips_original))]

        if max_grad_norm is not None:
            # Clip the gradients (normalize)
            grads_pi, _grad_norm = tf.clip_by_global_norm(grads_pi, max_grad_norm)
            grads_pi_lips_regu, _grad_norm_lips_regu = tf.clip_by_global_norm(grads_pi_lips_regu, max_grad_norm)
            grads_vf, _grad_norm = tf.clip_by_global_norm(grads_vf, max_grad_norm)
        grads_and_var_pi = list(zip(grads_pi, var_pi))
        grads_and_var_pi_lips_regu = list(zip(grads_pi_lips_regu, var_pi_lips_regu))
        grads_and_var_vf = list(zip(grads_vf, var_vf))

        self._train_op_pi = self.trainer_pi.apply_gradients(grads_and_var_pi)
        self._train_op_pi_lips_regu = self.trainer_pi_lips_regu.apply_gradients(grads_and_var_pi_lips_regu)
        self._train_op_vf = self.trainer_vf.apply_gradients(grads_and_var_vf)

        if attack_args.training_type == 'attack':
            self.loss_names = ['policy_loss_pi', 'lipschitz_loss', 'value_loss', 'policy_entropy', 'approxkl', 'clipfrac']
            self.stats_list = [pg_loss_normal, lipschitz_loss, vf_loss, entropy, approxkl, clipfrac]
        if attack_args.training_type == 'normal':
            self.loss_names = ['policy_loss_pi', 'value_loss', 'policy_entropy', 'approxkl', 'clipfrac']
            self.stats_list = [pg_loss_normal, vf_loss, entropy, approxkl, clipfrac]

        self.train_model = train_model
        self.act_model = act_model
        self.step = act_model.step
        self.step_attacked = act_model.step_attacked
        self.step_eval = act_model.step_eval
        self.step_attacked_eval = act_model.step_attacked_eval
        # self.step_policy_adversaries = act_model.step_policy_adversaries
        self.get_adversaries = act_model.get_adversaries
        # self.get_other_adversaries = act_model.get_other_adversaries
        self.value = act_model.value
        self.initial_state = act_model.initial_state

        self.save = functools.partial(save_variables, sess=sess)
        self.load = functools.partial(load_variables, sess=sess)
        initialize()
        # print("trainable variables", self.sess.run(tf.trainable_variables()))

        global_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="")
        if MPI is not None:
            sync_from_root(sess, global_variables, comm=comm) #pylint: disable=E1101

    def get_trainable_variables(self):
        return self.sess.run(tf.trainable_variables(scope='ppo2_model/pi'))

    def kl_div(self, dist1, dist2):
        kl = tf.reduce_sum(dist1 * tf.log(dist1/dist2), 1)
        return kl

    def train(self, lr, cliprange, obs, returns, masks, actions, values,
              neglogpacs, states=None):
        # Here we calculate advantage A(s,a) = R + yV(s') - V(s)
        # Returns = R + yV(s')
        advs = returns - values
        # Normalize the advantages
        advs = (advs - advs.mean()) / (advs.std() + 1e-8)

        td_map = {
            self.train_model.X: obs,
            self.A: actions,
            self.ADV: advs,
            self.R: returns,
            self.LR: lr,
            self.CLIPRANGE: cliprange,
            self.OLDNEGLOGPAC: neglogpacs,
            self.OLDVPRED: values
        }

        if states is not None:
            td_map[self.train_model.S] = states
            td_map[self.train_model.M] = masks

        if self.attack_args.training_type == 'attack':
            if self.attack_args.test:
                stas, pi_norm, pi_lips_norm, pi_lips_original_norm, ent_norm = self.sess.run([self.stats_list,
                                                                                        self.grads_pi_norm,
                                            self.grads_pi_lips_norm,
                                            self.grads_pi_lips_original_norm,
                                            self.grads_ent_norm], td_map)
                # print("stas", stas)
                # print("norm norms", pi_norm)
                # print("lips norms", pi_lips_norm)
                # print("lips original norm", pi_lips_original_norm)
                # print("entr norm", ent_norm)
                logger.log("stas", stas)
                logger.log("norm norm", pi_norm)
                logger.log("entr norm", ent_norm)
                logger.log("lips norm", pi_lips_norm)
                logger.log("lips original norm", pi_lips_original_norm)

            self.sess.run([self._train_op_vf, self._train_op_pi_lips_regu], td_map)

            if self.attack_args.test:
                param_norms = self.sess.run(self.pi_norm, td_map)
                logger.log("params norms", param_norms)
            return self.sess.run(self.stats_list, td_map)

        elif self.attack_args.training_type == 'normal':
            self.sess.run([self._train_op_vf] + [self._train_op_pi], td_map)
            return self.sess.run(self.stats_list, td_map)


