import numpy as np
import tensorflow as tf
from baselines.a2c import utils
from baselines.a2c.utils import conv, crop_and_concat, deconv2d, fc, conv_to_fc, batch_to_seq, seq_to_batch, max_pool
from baselines.common.mpi_running_mean_std import RunningMeanStd
import tensorflow.contrib.layers as layers
from collections import OrderedDict

from baselines.a2c.utils_1 import *

mapping = {}

def register(name):
    def _thunk(func):
        mapping[name] = func
        return func
    return _thunk


def nature_cnn(unscaled_images, network_size=1, **conv_kwargs):
    """
    CNN from Nature paper.
    """
    scaled_images = tf.cast(unscaled_images, tf.float32) / 255.
    activ = tf.nn.relu
    h = activ(conv(scaled_images, 'c1', nf=32*network_size, rf=8, stride=4, init_scale=np.sqrt(2),
                   **conv_kwargs))
    h2 = activ(conv(h, 'c2', nf=64*network_size, rf=4, stride=2, init_scale=np.sqrt(2), **conv_kwargs))
    h3 = activ(conv(h2, 'c3', nf=64*network_size, rf=3, stride=1, init_scale=np.sqrt(2), **conv_kwargs))
    h3 = conv_to_fc(h3)
    return activ(fc(h3, 'fc1', nh=512*network_size, init_scale=np.sqrt(2)))


@register("mlp")
def mlp(num_layers=2, num_hidden=64, activation=tf.tanh, layer_norm=False):
    """
    Stack of fully-connected layers to be used in a policy / q-function approximator

    Parameters:
    ----------

    num_layers: int                 number of fully-connected layers (default: 2)

    num_hidden: int                 size of fully-connected layers (default: 64)

    activation:                     activation function (default: tf.tanh)

    Returns:
    -------

    function that builds fully connected network with a given input tensor / placeholder
    """
    def network_fn(X):
        h = tf.layers.flatten(X)
        for i in range(num_layers):
            h = fc(h, 'mlp_fc{}'.format(i), nh=num_hidden, init_scale=np.sqrt(2))
            if layer_norm:
                h = tf.contrib.layers.layer_norm(h, center=True, scale=True)
            h = activation(h)

        return h

    return network_fn


@register("attack_net_mlp")
def attack_net_mlp(stack_attack_network=1, num_layers=2, num_hidden=64, activation=tf.tanh, layer_norm=False):
    """
    Stack of fully-connected layers to be used in a policy / q-function approximator

    Parameters:
    ----------

    num_layers: int                 number of fully-connected layers (default: 2)

    num_hidden: int                 size of fully-connected layers (default: 64)

    activation:                     activation function (default: tf.tanh)

    Returns:
    -------

    function that builds fully connected network with a given input tensor / placeholder
    """
    num_hidden = [256, 128, 64, 32, 16]

    def network_fn(X):
        hidden_layers = []
        h = tf.layers.flatten(X)
        shape_of_input = h.get_shape()[1]
        # print("shape of input", shape_of_input)
        for i in range(len(num_hidden)):
            h = fc(h, 'mlp_fc{}'.format(i), nh=num_hidden[i], init_scale=np.sqrt(2))
            if layer_norm:
                h = tf.contrib.layers.layer_norm(h, center=True, scale=True)
            hidden_layers.append(h)
            h = activation(h)

        print("hidden layers", hidden_layers)

        for i in reversed(range(0, len(num_hidden)-1)):
            print("h before", h)
            h = fc(h, 'mlp_fc{}_reverse'.format(i), nh=num_hidden[i], init_scale=np.sqrt(2))
            if layer_norm:
                h = tf.contrib.layers.layer_norm(h, center=True, scale=True)
            print("h after", h, num_hidden[i], i)
            h = h + hidden_layers[i]
            h = activation(h)

        h = fc(h, 'mlp_fc{}_reverse'.format('out'), nh=shape_of_input, init_scale=np.sqrt(2))

        return h

    return network_fn

@register("cnn")
def cnn(**conv_kwargs):
    def network_fn(X):
        print("cnn")
        return nature_cnn(X, **conv_kwargs)
    return network_fn


@register("cnn_small")
def cnn_small(network_size=1, **conv_kwargs):
    def network_fn(X):
        print("using small cnn network")
        h = tf.cast(X, tf.float32) / 255.

        activ = tf.nn.relu
        h = activ(conv(h, 'c1', nf=8 * network_size, rf=8, stride=4, init_scale=np.sqrt(2), **conv_kwargs))
        h = activ(conv(h, 'c2', nf=16 * network_size, rf=4, stride=2, init_scale=np.sqrt(2), **conv_kwargs))
        h = conv_to_fc(h)
        h = activ(fc(h, 'fc1', nh=128 * network_size, init_scale=np.sqrt(2)))
        return h
    return network_fn


@register("lstm")
def lstm(nlstm=128, layer_norm=False):
    """
    Builds LSTM (Long-Short Term Memory) network to be used in a policy.
    Note that the resulting function returns not only the output of the LSTM
    (i.e. hidden state of lstm for each step in the sequence), but also a dictionary
    with auxiliary tensors to be set as policy attributes.

    Specifically,
        S is a placeholder to feed current state (LSTM state has to be managed outside policy)
        M is a placeholder for the mask (used to mask out observations after the end of the episode, but can be used for other purposes too)
        initial_state is a numpy array containing initial lstm state (usually zeros)
        state is the output LSTM state (to be fed into S at the next call)


    An example of usage of lstm-based policy can be found here: common/tests/test_doc_examples.py/test_lstm_example

    Parameters:
    ----------

    nlstm: int          LSTM hidden state size

    layer_norm: bool    if True, layer-normalized version of LSTM is used

    Returns:
    -------

    function that builds LSTM with a given input tensor / placeholder
    """

    def network_fn(X, nenv=1):
        nbatch = X.shape[0]
        nsteps = nbatch // nenv

        h = tf.layers.flatten(X)

        M = tf.placeholder(tf.float32, [nbatch]) #mask (done t-1)
        S = tf.placeholder(tf.float32, [nenv, 2*nlstm]) #states

        xs = batch_to_seq(h, nenv, nsteps)
        ms = batch_to_seq(M, nenv, nsteps)

        if layer_norm:
            h5, snew = utils.lnlstm(xs, ms, S, scope='lnlstm', nh=nlstm)
        else:
            h5, snew = utils.lstm(xs, ms, S, scope='lstm', nh=nlstm)

        h = seq_to_batch(h5)
        initial_state = np.zeros(S.shape.as_list(), dtype=float)

        return h, {'S':S, 'M':M, 'state':snew, 'initial_state':initial_state}

    return network_fn


@register("cnn_lstm")
def cnn_lstm(nlstm=128, layer_norm=False, **conv_kwargs):
    def network_fn(X, nenv=1):
        nbatch = X.shape[0]
        nsteps = nbatch // nenv

        h = nature_cnn(X, **conv_kwargs)

        M = tf.placeholder(tf.float32, [nbatch]) #mask (done t-1)
        S = tf.placeholder(tf.float32, [nenv, 2*nlstm]) #states

        xs = batch_to_seq(h, nenv, nsteps)
        ms = batch_to_seq(M, nenv, nsteps)

        if layer_norm:
            h5, snew = utils.lnlstm(xs, ms, S, scope='lnlstm', nh=nlstm)
        else:
            h5, snew = utils.lstm(xs, ms, S, scope='lstm', nh=nlstm)

        h = seq_to_batch(h5)
        initial_state = np.zeros(S.shape.as_list(), dtype=float)

        return h, {'S':S, 'M':M, 'state':snew, 'initial_state':initial_state}

    return network_fn


@register("cnn_lnlstm")
def cnn_lnlstm(nlstm=128, **conv_kwargs):
    return cnn_lstm(nlstm, layer_norm=True, **conv_kwargs)


@register("conv_only")
def conv_only(network_size=1, **conv_kwargs):
    convs = [(network_size*32, 8, 4), (network_size*64, 4, 2), (network_size*64, 3, 1)]
    '''
    convolutions-only net

    Parameters:
    ----------

    conv:       list of triples (filter_number, filter_size, stride) specifying parameters for each layer.

    Returns:

    function that takes tensorflow tensor as input and returns the output of the last convolutional layer

    '''

    def network_fn(X):
        out = tf.cast(X, tf.float32) / 255.
        with tf.variable_scope("convnet"):
            for num_outputs, kernel_size, stride in convs:
                out = layers.convolution2d(out,
                                           num_outputs=num_outputs,
                                           kernel_size=kernel_size,
                                           stride=stride,
                                           activation_fn=tf.nn.relu,
                                           **conv_kwargs)

        return out
    return network_fn


@register("attack_net")
def attack_net(stack_attack_network=1, **conv_kwargs):
    def _pad(input_x, filter_size):
        amount = filter_size - 1
        return tf.pad(input_x,
                      [[0, 0], [amount, amount], [amount, amount], [0, 0]])

    def stack_unit(scaled_images, i):
        scaled_images = _pad(scaled_images, 4)
        # print("padded X", scaled_images.shape)
        activ = tf.nn.tanh
        h = activ(conv(scaled_images, 'c'+str(i*2+1), nf=32, rf=4, stride=1, pad='VALID', init_scale=np.sqrt(2),
                       **conv_kwargs))
        # print("h", h.shape)
        h1 = activ(conv(h, 'c'+str(i*2+2), nf=4, rf=4, stride=1, pad='VALID', init_scale=np.sqrt(2),
                        **conv_kwargs))
        # print("h1", h1.shape)
        return h1

    def network_fn(X):
        scaled_images = tf.cast(X, tf.float32) / 255.
        scaled_images = 2 * (scaled_images - 0.5)
        for i in range(stack_attack_network):
            scaled_images = stack_unit(scaled_images, i)
            # print("shape check", scaled_images.shape, i)
        return scaled_images
    return network_fn


@register("Unet")
def Unet(stack_attack_network=0, **conv_kwargs):

    def _pad(input_x, amount):
        return tf.pad(input_x,
                      [[0, 0], [amount, amount], [amount, amount], [0, 0]])

    def generator0(image):

        g_bn_e2 = batch_norm_i2i(name='g_bn_e2')
        g_bn_e3 = batch_norm_i2i(name='g_bn_e3')
        g_bn_e4 = batch_norm_i2i(name='g_bn_e4')
        g_bn_e5 = batch_norm_i2i(name='g_bn_e5')
        g_bn_e6 = batch_norm_i2i(name='g_bn_e6')
        g_bn_e7 = batch_norm_i2i(name='g_bn_e7')
        g_bn_e8 = batch_norm_i2i(name='g_bn_e8')

        g_bn_d1 = batch_norm_i2i(name='g_bn_d1')
        g_bn_d2 = batch_norm_i2i(name='g_bn_d2')
        g_bn_d3 = batch_norm_i2i(name='g_bn_d3')
        g_bn_d4 = batch_norm_i2i(name='g_bn_d4')
        g_bn_d5 = batch_norm_i2i(name='g_bn_d5')
        g_bn_d6 = batch_norm_i2i(name='g_bn_d6')
        g_bn_d7 = batch_norm_i2i(name='g_bn_d7')

        s = 96
        gf_dim = 16
        batch_size = image.get_shape()[0].value
        output_c_dim = image.get_shape()[-1].value
        input_c_dim = image.get_shape()[-1].value

        # image = tf.zeros([batch_size, 84, 84, input_c_dim])

        image = _pad(image, 6)

        s2, s4, s8, s16, s32, s64, s128 = int(s / 2), int(s / 4), int(s / 8), int(s / 16), int(s / 32), int(
            s / 64), int(s / 128)

        print("s2, s4, s8, s16, s32, s64, s128", s2, s4, s8, s16, s32, s64, s128)

        # image is (256 x 256 x input_c_dim)
        print("image", image.shape)
        e1 = conv2d_i2i(image, gf_dim, name='g_e1_conv')
        print("e1", e1.shape)
        # e1 is (128 x 128 x gf_dim)
        e2 = g_bn_e2(conv2d_i2i(lrelu_i2i(e1), gf_dim * 2, name='g_e2_conv'))
        print("e2", e2.shape)
        # e2 is (64 x 64 x gf_dim*2)
        e3 = g_bn_e3(conv2d_i2i(lrelu_i2i(e2), gf_dim * 4, name='g_e3_conv'))
        print("e3", e3.shape)
        # e3 is (32 x 32 x gf_dim*4)
        e4 = g_bn_e4(conv2d_i2i(lrelu_i2i(e3), gf_dim * 8, name='g_e4_conv'))
        print("e4", e4.shape)
        # e4 is (16 x 16 x gf_dim*8)
        e5 = g_bn_e5(conv2d_i2i(lrelu_i2i(e4), gf_dim * 8, name='g_e5_conv'))
        print("e5", e5.shape)
        # e5 is (8 x 8 x gf_dim*8)
        e6 = g_bn_e6(conv2d_i2i(lrelu_i2i(e5), gf_dim * 8, name='g_e6_conv'))
        print("e6", e6.shape)
        # e6 is (4 x 4 x gf_dim*8)
        # e7 = g_bn_e7(conv2d_i2i(lrelu_i2i(e6), gf_dim * 8, name='g_e7_conv'))
        # print("e7", e7.shape)
        # # e7 is (2 x 2 x gf_dim*8)
        # e8 = g_bn_e8(conv2d_i2i(lrelu_i2i(e7), gf_dim * 8, name='g_e8_conv'))
        # print("e8", e8.shape)
        # # e8 is (1 x 1 x gf_dim*8)

        # d1, d1_w, d1_b = deconv2d_i2i(tf.nn.relu(e8),
        #                               [batch_size, s128, s128, gf_dim * 8], name='g_d1',
        #                               with_w=True)
        # d1 = tf.nn.dropout(g_bn_d1(d1), 0.5)
        # print("d1", d1.shape)
        # d1 = tf.concat([d1, e7], 3)
        # print("d1", d1.shape)
        # # d1 is (2 x 2 x gf_dim*8*2)
        #
        # d2, d2_w, d2_b = deconv2d_i2i(tf.nn.relu(d1),
        #                               [batch_size, s64, s64, gf_dim * 8], name='g_d2',
        #                               with_w=True)
        # d2 = tf.nn.dropout(g_bn_d2(d2), 0.5)
        # print("d2", d2.shape)
        # d2 = tf.concat([d2, e6], 3)
        # print("d2", d2.shape)
        # # d2 is (4 x 4 x gf_dim*8*2)

        d3, d3_w, d3_b = deconv2d_i2i(tf.nn.relu(e6),
                                      [batch_size, s32, s32, gf_dim * 8], name='g_d3',
                                      with_w=True)
        d3 = tf.nn.dropout(g_bn_d3(d3), 0.5)
        print("d3", d3.shape)
        d3 = tf.concat([d3, e5], 3)
        print("d3", d3.shape)
        # d3 is (8 x 8 x gf_dim*8*2)

        d4, d4_w, d4_b = deconv2d_i2i(tf.nn.relu(d3),
                                      [batch_size, s16, s16, gf_dim * 8], name='g_d4',
                                      with_w=True)
        d4 = g_bn_d4(d4)
        print("d4", d4.shape)
        d4 = tf.concat([d4, e4], 3)
        print("d4", d4.shape)
        # d4 is (16 x 16 x gf_dim*8*2)

        d5, d5_w, d5_b = deconv2d_i2i(tf.nn.relu(d4),
                                      [batch_size, s8, s8, gf_dim * 4], name='g_d5',
                                      with_w=True)
        d5 = g_bn_d5(d5)
        print("d5", d5.shape)
        d5 = tf.concat([d5, e3], 3)
        print("d5", d5.shape)
        # d5 is (32 x 32 x gf_dim*4*2)

        d6, d6_w, d6_b = deconv2d_i2i(tf.nn.relu(d5),
                                      [batch_size, s4, s4, gf_dim * 2], name='g_d6',
                                      with_w=True)
        d6 = g_bn_d6(d6)
        print("d6", d6.shape)
        d6 = tf.concat([d6, e2], 3)
        print("d6", d6.shape)
        # d6 is (64 x 64 x gf_dim*2*2)

        d7, d7_w, d7_b = deconv2d_i2i(tf.nn.relu(d6),
                                      [batch_size, s2, s2, gf_dim], name='g_d7', with_w=True)
        d7 = g_bn_d7(d7)
        print("d7", d7.shape)
        d7 = tf.concat([d7, e1], 3)
        print("d7", d7.shape)
        # d7 is (128 x 128 x gf_dim*1*2)

        d8, d8_w, d8_b = deconv2d_i2i(tf.nn.relu(d7),
                                      [batch_size, s, s, output_c_dim], name='g_d8',
                                      with_w=True)
        print("d8", d8.shape)
        # d8 is (256 x 256 x output_c_dim)

        final = d8[:, 6:90, 6:90, :]
        # print("final", final.shape)

        return tf.nn.tanh(final)


    def generator1(image):

        g_bn_e2 = batch_norm_i2i(name='g_bn_e2')
        g_bn_e3 = batch_norm_i2i(name='g_bn_e3')
        g_bn_e4 = batch_norm_i2i(name='g_bn_e4')
        g_bn_e5 = batch_norm_i2i(name='g_bn_e5')
        g_bn_e6 = batch_norm_i2i(name='g_bn_e6')
        g_bn_e7 = batch_norm_i2i(name='g_bn_e7')
        g_bn_e8 = batch_norm_i2i(name='g_bn_e8')

        g_bn_d1 = batch_norm_i2i(name='g_bn_d1')
        g_bn_d2 = batch_norm_i2i(name='g_bn_d2')
        g_bn_d3 = batch_norm_i2i(name='g_bn_d3')
        g_bn_d4 = batch_norm_i2i(name='g_bn_d4')
        g_bn_d5 = batch_norm_i2i(name='g_bn_d5')
        g_bn_d6 = batch_norm_i2i(name='g_bn_d6')
        g_bn_d7 = batch_norm_i2i(name='g_bn_d7')

        s = 128
        gf_dim = 16
        batch_size = image.get_shape()[0].value
        output_c_dim = image.get_shape()[-1].value
        input_c_dim = image.get_shape()[-1].value

        # image = tf.zeros([batch_size, 128, 128, input_c_dim])
        image = _pad(image, 22)

        s2, s4, s8, s16, s32, s64, s128 = int(s / 2), int(s / 4), int(s / 8), int(s / 16), int(s / 32), int(
            s / 64), int(s / 128)

        print("s2, s4, s8, s16, s32, s64, s128", s2, s4, s8, s16, s32, s64, s128)

        # image is (256 x 256 x input_c_dim)
        print("image", image.shape)
        e1 = conv2d_i2i(image, gf_dim, k_h=4, k_w=4, name='g_e1_conv')
        print("e1", e1.shape)
        # e1 is (128 x 128 x gf_dim)
        e2 = g_bn_e2(conv2d_i2i(lrelu_i2i(e1), gf_dim * 2, k_h=4, k_w=4, name='g_e2_conv'))
        print("e2", e2.shape)
        # e2 is (64 x 64 x gf_dim*2)
        e3 = g_bn_e3(conv2d_i2i(lrelu_i2i(e2), gf_dim * 4, k_h=4, k_w=4, name='g_e3_conv'))
        print("e3", e3.shape)
        # e3 is (32 x 32 x gf_dim*4)
        e4 = g_bn_e4(conv2d_i2i(lrelu_i2i(e3), gf_dim * 8, k_h=4, k_w=4, name='g_e4_conv'))
        print("e4", e4.shape)
        # e4 is (16 x 16 x gf_dim*8)
        e5 = g_bn_e5(conv2d_i2i(lrelu_i2i(e4), gf_dim * 8, k_h=4, k_w=4, name='g_e5_conv'))
        print("e5", e5.shape)
        # e5 is (8 x 8 x gf_dim*8)
        e6 = g_bn_e6(conv2d_i2i(lrelu_i2i(e5), gf_dim * 8, k_h=4, k_w=4, name='g_e6_conv'))
        print("e6", e6.shape)
        # e6 is (4 x 4 x gf_dim*8)
        e7 = g_bn_e7(conv2d_i2i(lrelu_i2i(e6), gf_dim * 8, k_h=4, k_w=4, name='g_e7_conv'))
        print("e7", e7.shape)
        # e7 is (2 x 2 x gf_dim*8)
        # e8 = g_bn_e8(conv2d_i2i(lrelu_i2i(e7), gf_dim * 8, k_h=4, k_w=4, name='g_e8_conv'))
        # print("e8", e8.shape)
        # # e8 is (1 x 1 x gf_dim*8)

        # d1, d1_w, d1_b = deconv2d_i2i(tf.nn.relu(e8),
        #                               [batch_size, s128, s128, gf_dim * 8], k_h=4, k_w=4, name='g_d1',
        #                               with_w=True)
        # d1 = tf.nn.dropout(g_bn_d1(d1), 0.5)
        # print("d1", d1.shape)
        # d1 = tf.concat([d1, e7], 3)
        # print("d1", d1.shape)
        # # d1 is (2 x 2 x gf_dim*8*2)
        #
        d2, d2_w, d2_b = deconv2d_i2i(tf.nn.relu(e7),
                                      [batch_size, s64, s64, gf_dim * 8], k_h=4, k_w=4, name='g_d2',
                                      with_w=True)
        d2 = tf.nn.dropout(g_bn_d2(d2), 0.5)
        print("d2", d2.shape)
        d2 = tf.concat([d2, e6], 3)
        print("d2", d2.shape)
        # d2 is (4 x 4 x gf_dim*8*2)

        d3, d3_w, d3_b = deconv2d_i2i(tf.nn.relu(d2),
                                      [batch_size, s32, s32, gf_dim * 8], k_h=4, k_w=4, name='g_d3',
                                      with_w=True)
        d3 = tf.nn.dropout(g_bn_d3(d3), 0.5)
        print("d3", d3.shape)
        d3 = tf.concat([d3, e5], 3)
        print("d3", d3.shape)
        # d3 is (8 x 8 x gf_dim*8*2)

        d4, d4_w, d4_b = deconv2d_i2i(tf.nn.relu(d3),
                                      [batch_size, s16, s16, gf_dim * 8], k_h=4, k_w=4, name='g_d4',
                                      with_w=True)
        d4 = g_bn_d4(d4)
        print("d4", d4.shape)
        d4 = tf.concat([d4, e4], 3)
        print("d4", d4.shape)
        # d4 is (16 x 16 x gf_dim*8*2)

        d5, d5_w, d5_b = deconv2d_i2i(tf.nn.relu(d4),
                                      [batch_size, s8, s8, gf_dim * 4], k_h=4, k_w=4, name='g_d5',
                                      with_w=True)
        d5 = g_bn_d5(d5)
        print("d5", d5.shape)
        d5 = tf.concat([d5, e3], 3)
        print("d5", d5.shape)
        # d5 is (32 x 32 x gf_dim*4*2)

        d6, d6_w, d6_b = deconv2d_i2i(tf.nn.relu(d5),
                                      [batch_size, s4, s4, gf_dim * 2], k_h=4, k_w=4, name='g_d6',
                                      with_w=True)
        d6 = g_bn_d6(d6)
        print("d6", d6.shape)
        d6 = tf.concat([d6, e2], 3)
        print("d6", d6.shape)
        # d6 is (64 x 64 x gf_dim*2*2)

        d7, d7_w, d7_b = deconv2d_i2i(tf.nn.relu(d6),
                                      [batch_size, s2, s2, gf_dim], k_h=4, k_w=4, name='g_d7', with_w=True)
        d7 = g_bn_d7(d7)
        print("d7", d7.shape)
        d7 = tf.concat([d7, e1], 3)
        print("d7", d7.shape)
        # d7 is (128 x 128 x gf_dim*1*2)

        d8, d8_w, d8_b = deconv2d_i2i(tf.nn.relu(d7),
                                      [batch_size, s, s, output_c_dim], k_h=4, k_w=4, name='g_d8',
                                      with_w=True)

        print("d8", d8.shape)
        # d8 is (256 x 256 x output_c_dim)

        final = d8[:, 22:106, 22:106, :]
        print("final", final.shape)

        return tf.nn.tanh(final)

    def generator2(image):

        g_bn_e2 = batch_norm_i2i(name='g_bn_e2')
        g_bn_e3 = batch_norm_i2i(name='g_bn_e3')
        g_bn_e4 = batch_norm_i2i(name='g_bn_e4')
        g_bn_e5 = batch_norm_i2i(name='g_bn_e5')
        g_bn_e6 = batch_norm_i2i(name='g_bn_e6')
        g_bn_e7 = batch_norm_i2i(name='g_bn_e7')
        g_bn_e8 = batch_norm_i2i(name='g_bn_e8')

        g_bn_d1 = batch_norm_i2i(name='g_bn_d1')
        g_bn_d2 = batch_norm_i2i(name='g_bn_d2')
        g_bn_d3 = batch_norm_i2i(name='g_bn_d3')
        g_bn_d4 = batch_norm_i2i(name='g_bn_d4')
        g_bn_d5 = batch_norm_i2i(name='g_bn_d5')
        g_bn_d6 = batch_norm_i2i(name='g_bn_d6')
        g_bn_d7 = batch_norm_i2i(name='g_bn_d7')

        s = 96
        gf_dim = 8
        batch_size = image.get_shape()[0].value
        output_c_dim = image.get_shape()[-1].value
        input_c_dim = image.get_shape()[-1].value

        # image = tf.zeros([batch_size, 84, 84, input_c_dim])

        image = _pad(image, 6)

        s2, s4, s8, s16, s32, s64, s128 = int(s / 2), int(s / 4), int(s / 8), int(s / 16), int(s / 32), int(
            s / 64), int(s / 128)

        print("s2, s4, s8, s16, s32, s64, s128", s2, s4, s8, s16, s32, s64, s128)

        # image is (256 x 256 x input_c_dim)
        print("image", image.shape)
        e1 = conv2d_i2i(image, gf_dim, name='g_e1_conv')
        print("e1", e1.shape)
        # e1 is (128 x 128 x gf_dim)
        e2 = g_bn_e2(conv2d_i2i(lrelu_i2i(e1), gf_dim * 2, name='g_e2_conv'))
        print("e2", e2.shape)
        # e2 is (64 x 64 x gf_dim*2)
        e3 = g_bn_e3(conv2d_i2i(lrelu_i2i(e2), gf_dim * 4, name='g_e3_conv'))
        print("e3", e3.shape)
        # e3 is (32 x 32 x gf_dim*4)
        e4 = g_bn_e4(conv2d_i2i(lrelu_i2i(e3), gf_dim * 8, name='g_e4_conv'))
        print("e4", e4.shape)
        # e4 is (16 x 16 x gf_dim*8)
        e5 = g_bn_e5(conv2d_i2i(lrelu_i2i(e4), gf_dim * 8, name='g_e5_conv'))
        print("e5", e5.shape)
        # e5 is (8 x 8 x gf_dim*8)
        e6 = g_bn_e6(conv2d_i2i(lrelu_i2i(e5), gf_dim * 8, name='g_e6_conv'))
        print("e6", e6.shape)
        # e6 is (4 x 4 x gf_dim*8)
        # e7 = g_bn_e7(conv2d_i2i(lrelu_i2i(e6), gf_dim * 8, name='g_e7_conv'))
        # print("e7", e7.shape)
        # # e7 is (2 x 2 x gf_dim*8)
        # e8 = g_bn_e8(conv2d_i2i(lrelu_i2i(e7), gf_dim * 8, name='g_e8_conv'))
        # print("e8", e8.shape)
        # # e8 is (1 x 1 x gf_dim*8)

        # d1, d1_w, d1_b = deconv2d_i2i(tf.nn.relu(e8),
        #                               [batch_size, s128, s128, gf_dim * 8], name='g_d1',
        #                               with_w=True)
        # d1 = tf.nn.dropout(g_bn_d1(d1), 0.5)
        # print("d1", d1.shape)
        # d1 = tf.concat([d1, e7], 3)
        # print("d1", d1.shape)
        # # d1 is (2 x 2 x gf_dim*8*2)
        #
        # d2, d2_w, d2_b = deconv2d_i2i(tf.nn.relu(d1),
        #                               [batch_size, s64, s64, gf_dim * 8], name='g_d2',
        #                               with_w=True)
        # d2 = tf.nn.dropout(g_bn_d2(d2), 0.5)
        # print("d2", d2.shape)
        # d2 = tf.concat([d2, e6], 3)
        # print("d2", d2.shape)
        # # d2 is (4 x 4 x gf_dim*8*2)

        d3, d3_w, d3_b = deconv2d_i2i(tf.nn.relu(e6),
                                      [batch_size, s32, s32, gf_dim * 8], name='g_d3',
                                      with_w=True)
        d3 = tf.nn.dropout(g_bn_d3(d3), 0.5)
        print("d3", d3.shape)
        d3 = tf.concat([d3, e5], 3)
        print("d3", d3.shape)
        # d3 is (8 x 8 x gf_dim*8*2)

        d4, d4_w, d4_b = deconv2d_i2i(tf.nn.relu(d3),
                                      [batch_size, s16, s16, gf_dim * 8], name='g_d4',
                                      with_w=True)
        d4 = g_bn_d4(d4)
        print("d4", d4.shape)
        d4 = tf.concat([d4, e4], 3)
        print("d4", d4.shape)
        # d4 is (16 x 16 x gf_dim*8*2)

        d5, d5_w, d5_b = deconv2d_i2i(tf.nn.relu(d4),
                                      [batch_size, s8, s8, gf_dim * 4], name='g_d5',
                                      with_w=True)
        d5 = g_bn_d5(d5)
        print("d5", d5.shape)
        d5 = tf.concat([d5, e3], 3)
        print("d5", d5.shape)
        # d5 is (32 x 32 x gf_dim*4*2)

        d6, d6_w, d6_b = deconv2d_i2i(tf.nn.relu(d5),
                                      [batch_size, s4, s4, gf_dim * 2], name='g_d6',
                                      with_w=True)
        d6 = g_bn_d6(d6)
        print("d6", d6.shape)
        d6 = tf.concat([d6, e2], 3)
        print("d6", d6.shape)
        # d6 is (64 x 64 x gf_dim*2*2)

        d7, d7_w, d7_b = deconv2d_i2i(tf.nn.relu(d6),
                                      [batch_size, s2, s2, gf_dim], name='g_d7', with_w=True)
        d7 = g_bn_d7(d7)
        print("d7", d7.shape)
        d7 = tf.concat([d7, e1], 3)
        print("d7", d7.shape)
        # d7 is (128 x 128 x gf_dim*1*2)

        d8, d8_w, d8_b = deconv2d_i2i(tf.nn.relu(d7),
                                      [batch_size, s, s, output_c_dim], name='g_d8',
                                      with_w=True)
        print("d8", d8.shape)
        # d8 is (256 x 256 x output_c_dim)

        final = d8[:, 6:90, 6:90, :]
        # print("final", final.shape)

        return tf.nn.tanh(final)

    def generator3(image):

        g_bn_e2 = batch_norm_i2i(name='g_bn_e2')
        g_bn_e3 = batch_norm_i2i(name='g_bn_e3')
        g_bn_e4 = batch_norm_i2i(name='g_bn_e4')
        g_bn_e5 = batch_norm_i2i(name='g_bn_e5')
        g_bn_e6 = batch_norm_i2i(name='g_bn_e6')
        g_bn_e7 = batch_norm_i2i(name='g_bn_e7')
        g_bn_e8 = batch_norm_i2i(name='g_bn_e8')

        g_bn_d1 = batch_norm_i2i(name='g_bn_d1')
        g_bn_d2 = batch_norm_i2i(name='g_bn_d2')
        g_bn_d3 = batch_norm_i2i(name='g_bn_d3')
        g_bn_d4 = batch_norm_i2i(name='g_bn_d4')
        g_bn_d5 = batch_norm_i2i(name='g_bn_d5')
        g_bn_d6 = batch_norm_i2i(name='g_bn_d6')
        g_bn_d7 = batch_norm_i2i(name='g_bn_d7')

        s = 96
        gf_dim = 4
        batch_size = image.get_shape()[0].value
        output_c_dim = image.get_shape()[-1].value
        input_c_dim = image.get_shape()[-1].value

        # image = tf.zeros([batch_size, 84, 84, input_c_dim])

        image = _pad(image, 6)

        s2, s4, s8, s16, s32, s64, s128 = int(s / 2), int(s / 4), int(s / 8), int(s / 16), int(s / 32), int(
            s / 64), int(s / 128)

        print("s2, s4, s8, s16, s32, s64, s128", s2, s4, s8, s16, s32, s64, s128)

        # image is (256 x 256 x input_c_dim)
        print("image", image.shape)
        e1 = conv2d_i2i(image, gf_dim, name='g_e1_conv')
        print("e1", e1.shape)
        # e1 is (128 x 128 x gf_dim)
        e2 = g_bn_e2(conv2d_i2i(lrelu_i2i(e1), gf_dim * 2, name='g_e2_conv'))
        print("e2", e2.shape)
        # e2 is (64 x 64 x gf_dim*2)
        e3 = g_bn_e3(conv2d_i2i(lrelu_i2i(e2), gf_dim * 4, name='g_e3_conv'))
        print("e3", e3.shape)
        # e3 is (32 x 32 x gf_dim*4)
        e4 = g_bn_e4(conv2d_i2i(lrelu_i2i(e3), gf_dim * 8, name='g_e4_conv'))
        print("e4", e4.shape)
        # e4 is (16 x 16 x gf_dim*8)
        e5 = g_bn_e5(conv2d_i2i(lrelu_i2i(e4), gf_dim * 8, name='g_e5_conv'))
        print("e5", e5.shape)
        # e5 is (8 x 8 x gf_dim*8)
        e6 = g_bn_e6(conv2d_i2i(lrelu_i2i(e5), gf_dim * 8, name='g_e6_conv'))
        print("e6", e6.shape)
        # e6 is (4 x 4 x gf_dim*8)
        # e7 = g_bn_e7(conv2d_i2i(lrelu_i2i(e6), gf_dim * 8, name='g_e7_conv'))
        # print("e7", e7.shape)
        # # e7 is (2 x 2 x gf_dim*8)
        # e8 = g_bn_e8(conv2d_i2i(lrelu_i2i(e7), gf_dim * 8, name='g_e8_conv'))
        # print("e8", e8.shape)
        # # e8 is (1 x 1 x gf_dim*8)

        # d1, d1_w, d1_b = deconv2d_i2i(tf.nn.relu(e8),
        #                               [batch_size, s128, s128, gf_dim * 8], name='g_d1',
        #                               with_w=True)
        # d1 = tf.nn.dropout(g_bn_d1(d1), 0.5)
        # print("d1", d1.shape)
        # d1 = tf.concat([d1, e7], 3)
        # print("d1", d1.shape)
        # # d1 is (2 x 2 x gf_dim*8*2)
        #
        # d2, d2_w, d2_b = deconv2d_i2i(tf.nn.relu(d1),
        #                               [batch_size, s64, s64, gf_dim * 8], name='g_d2',
        #                               with_w=True)
        # d2 = tf.nn.dropout(g_bn_d2(d2), 0.5)
        # print("d2", d2.shape)
        # d2 = tf.concat([d2, e6], 3)
        # print("d2", d2.shape)
        # # d2 is (4 x 4 x gf_dim*8*2)

        d3, d3_w, d3_b = deconv2d_i2i(tf.nn.relu(e6),
                                      [batch_size, s32, s32, gf_dim * 8], name='g_d3',
                                      with_w=True)
        d3 = tf.nn.dropout(g_bn_d3(d3), 0.5)
        print("d3", d3.shape)
        d3 = tf.concat([d3, e5], 3)
        print("d3", d3.shape)
        # d3 is (8 x 8 x gf_dim*8*2)

        d4, d4_w, d4_b = deconv2d_i2i(tf.nn.relu(d3),
                                      [batch_size, s16, s16, gf_dim * 8], name='g_d4',
                                      with_w=True)
        d4 = g_bn_d4(d4)
        print("d4", d4.shape)
        d4 = tf.concat([d4, e4], 3)
        print("d4", d4.shape)
        # d4 is (16 x 16 x gf_dim*8*2)

        d5, d5_w, d5_b = deconv2d_i2i(tf.nn.relu(d4),
                                      [batch_size, s8, s8, gf_dim * 4], name='g_d5',
                                      with_w=True)
        d5 = g_bn_d5(d5)
        print("d5", d5.shape)
        d5 = tf.concat([d5, e3], 3)
        print("d5", d5.shape)
        # d5 is (32 x 32 x gf_dim*4*2)

        d6, d6_w, d6_b = deconv2d_i2i(tf.nn.relu(d5),
                                      [batch_size, s4, s4, gf_dim * 2], name='g_d6',
                                      with_w=True)
        d6 = g_bn_d6(d6)
        print("d6", d6.shape)
        d6 = tf.concat([d6, e2], 3)
        print("d6", d6.shape)
        # d6 is (64 x 64 x gf_dim*2*2)

        d7, d7_w, d7_b = deconv2d_i2i(tf.nn.relu(d6),
                                      [batch_size, s2, s2, gf_dim], name='g_d7', with_w=True)
        d7 = g_bn_d7(d7)
        print("d7", d7.shape)
        d7 = tf.concat([d7, e1], 3)
        print("d7", d7.shape)
        # d7 is (128 x 128 x gf_dim*1*2)

        d8, d8_w, d8_b = deconv2d_i2i(tf.nn.relu(d7),
                                      [batch_size, s, s, output_c_dim], name='g_d8',
                                      with_w=True)
        print("d8", d8.shape)
        # d8 is (256 x 256 x output_c_dim)

        final = d8[:, 6:90, 6:90, :]
        # print("final", final.shape)

        return tf.nn.tanh(final)

    def create_conv_net(X, keep_prob=None, layers=3, features_root=64, filter_size=3, pool_size=2):
        """
        Creates a new convolutional unet for the given parametrization.
        :param x: input tensor, shape [?,nx,ny,channels]
        :param keep_prob: dropout probability tensor
        :param channels: number of channels in the input image
        :param n_class: number of output labels
        :param layers: number of layers in the net
        :param features_root: number of features in the first layer
        :param filter_size: size of the convolution filter
        :param pool_size: size of the max pooling operation
        :param summaries: Flag if summaries should be created
        """

        # Placeholder for the input image
        # X = _pad(X, filter_size)
        X = tf.zeros([8, 90, 90, 4])
        print("input", X.shape)
        with tf.name_scope("preprocessing"):
            nx = tf.shape(X)[1]
            ny = tf.shape(X)[2]
            # x_image = tf.reshape(X, tf.stack([-1, nx, ny, 4]))
            in_node = X
            batch_size = tf.shape(X)[0]

        # weights = []
        # biases = []
        convs = []
        pools = OrderedDict()
        deconv = OrderedDict()
        dw_h_convs = OrderedDict()
        up_h_convs = OrderedDict()

        scaled_images = tf.cast(X, tf.float32) / 255.

        in_size = 1000
        size = in_size
        # down layers

        for layer in range(0, layers):
            features = 2 ** layer * features_root
            stddev = np.sqrt(2 / (filter_size ** 2 * features))
            if layer == 0:
                # w1 = weight_variable([filter_size, filter_size, channels, features], stddev, name="w1")
                conv1 = conv(in_node, 'c1' + str(layer), nf=features, rf=filter_size, stride=1, pad='VALID',
                             init_scale=stddev,
                             **conv_kwargs)
            else:
                conv1 = conv(in_node, 'c1' + str(layer), nf=features, rf=filter_size, stride=1, pad='VALID',
                             init_scale=stddev, **conv_kwargs)

            print("conv1", conv1.shape)
            tmp_h_conv = tf.nn.relu(conv1)

            # w1 = weight_variable([filter_size, filter_size, features // 2, features], stddev, name="w1")

            # w2 = weight_variable([filter_size, filter_size, features, features], stddev, name="w2")
            # b1 = bias_variable([features], name="b1")
            # b2 = bias_variable([features], name="b2")

            conv2 = conv(tmp_h_conv, 'c2' + str(layer), nf=features, rf=filter_size, stride=1, pad='VALID',
                         init_scale=stddev, **conv_kwargs)

            print("conv2", conv2.shape)

            # conv1 = conv2d(in_node, w1, b1, keep_prob)
            # tmp_h_conv = tf.nn.relu(conv1)
            # conv2 = conv2d(tmp_h_conv, w2, b2, keep_prob)
            dw_h_convs[layer] = tf.nn.relu(conv2)
            # dw_h_convs[layer] = conv2
            # weights.append((w1, w2))
            # biases.append((b1, b2))
            convs.append((conv1, conv2))

            size -= 2 * 2 * (filter_size // 2)  # valid conv
            if layer < layers - 1:
                pools[layer] = max_pool(dw_h_convs[layer], pool_size, pad="VALID")

                print("pool", pools[layer].shape)

                in_node = pools[layer]
                size /= pool_size

        in_node = dw_h_convs[layers - 1]

        # up layers
        for layer in range(layers - 2, -1, -1):
            features = 2 ** (layer + 1) * features_root
            stddev = np.sqrt(2 / (filter_size ** 2 * features))

            # wd = weight_variable_devonc([pool_size, pool_size, features // 2, features], stddev, name="wd")
            # bd = bias_variable([features // 2], name="bd")

            # h_deconv = tf.nn.relu(deconv2d(in_node, wd, pool_size) + bd)
            h_deconv = deconv2d(in_node, 'cd' + str(layer), nf=features, rf=pool_size, stride=pool_size, pad='VALID',
                            init_scale=stddev, **conv_kwargs)
            print("h_deconv", h_deconv.shape)
            h_deconv_concat = crop_and_concat(dw_h_convs[layer], h_deconv)
            print("h_dconv_cont", dw_h_convs[layer].shape, h_deconv.shape, h_deconv_concat.shape)
            deconv[layer] = h_deconv_concat

            # w1 = weight_variable([filter_size, filter_size, features, features // 2], stddev, name="w1")
            # w2 = weight_variable([filter_size, filter_size, features // 2, features // 2], stddev, name="w2")
            # b1 = bias_variable([features // 2], name="b1")
            # b2 = bias_variable([features // 2], name="b2")

            # conv1 = conv(h_deconv_concat, w1, b1, keep_prob)

            conv1 = conv(h_deconv_concat, 'dc1' + str(layer), nf=features // 2, rf=filter_size, stride=1, pad='VALID',
                         init_scale=stddev, **conv_kwargs)

            print("dconv1", conv1.shape)

            h_conv = tf.nn.relu(conv1)
            # conv2 = conv(h_conv, w2, b2, keep_prob)
            conv2 = conv(h_conv, 'dc2' + str(layer), nf=features // 2, rf=filter_size, stride=1, pad='VALID',
                         init_scale=stddev, **conv_kwargs)

            print("dconv2", conv2.shape)

            in_node = tf.nn.relu(conv2)

            up_h_convs[layer] = in_node

            # weights.append((w1, w2))
            # biases.append((b1, b2))
            convs.append((conv1, conv2))

            size *= pool_size
            size -= 2 * 2 * (filter_size // 2)  # valid conv

        stddev = np.sqrt(2 / (filter_size ** 2 * features))
        in_node = conv(in_node, 'lastc', nf=X.get_shape()[-1].value, rf=1, stride=1, pad='VALID',
                         init_scale=stddev, **conv_kwargs)

        print("final", in_node.shape)

        # # Output Map
        # with tf.name_scope("output_map"):
        #     weight = weight_variable([1, 1, features_root, n_class], stddev)
        #     bias = bias_variable([n_class], name="bias")
        #     # conv = conv2d(in_node, weight, bias, tf.constant(1.0))
        #     last_conv = conv(in_node, 'lastc', nf=features // 2, rf=filter_size, stride=1, pad='VALID',
        #          init_scale=stddev, **conv_kwargs)
        #     output_map = tf.nn.relu(conv)
        #     up_h_convs["out"] = output_map

        return in_node

    def network_fn(X):
        pass
        # scaled_images = tf.cast(X, tf.float32) / 255.
        # # scaled_images = 2 * (scaled_images - 0.5)
        # activ = tf.nn.relu
        # h1 = activ(conv(scaled_images, 'c1', nf=32, rf=3, stride=1, pad='SAME', init_scale=np.sqrt(2),
        #                 **conv_kwargs))
        # h1_p = pool(h1, 2, pad='VALID')
        # print("h1", h1.shape, h1_p.shape)
        # h2 = activ(conv(h1_p, 'c2', nf=64, rf=3, stride=1, pad='SAME', init_scale=np.sqrt(2),
        #                 **conv_kwargs))
        # h2_p = pool(h2, 2, pad='VALID')
        # print("h2", h2.shape, h2_p.shape)
        # h3 = activ(conv(h2_p, 'c3', nf=128, rf=3, stride=1, pad='SAME', init_scale=np.sqrt(2),
        #                 **conv_kwargs))
        # h3_p = pool(h3, 2, pad='VALID')
        # print("h3", h3.shape, h3_p.shape)
        # h4 = activ(conv(h3_p, 'c4', nf=256, rf=3, stride=1, pad='SAME', init_scale=np.sqrt(2),
        #                 **conv_kwargs))
        # h4_p = pool(h4, 2, pad='VALID')
        # print("h4", h4.shape, h4_p.shape)
        # h5 = activ(conv(h4_p, 'c5', nf=512, rf=3, stride=1, pad='SAME', init_scale=np.sqrt(2),
        #                 **conv_kwargs))
        # h5_p = pool(h5, 2, pad='VALID')
        # print("h5", h5.shape, h5_p.shape)
        #
        # h6 = activ(conv_transpose(h5_p, 'ct6', nf=512, rf=3, stride=1, pad='SAME', init_scale=np.sqrt(2),
        #                 **conv_kwargs))
        # print("h6", h6.shape)
        #
        # h7 = activ(conv_transpose(h6, 'ct7', nf=256, rf=3, stride=1, pad='SAME', init_scale=np.sqrt(2),
        #                           **conv_kwargs))
        # print("h7", h7.shape)
        #
        # h8 = activ(conv_transpose(h7, 'ct8', nf=128, rf=3, stride=1, pad='SAME', init_scale=np.sqrt(2),
        #                           **conv_kwargs))
        # print("h8", h8.shape)
        #
        # h9 = activ(conv_transpose(h8, 'ct9', nf=64, rf=3, stride=1, pad='SAME', init_scale=np.sqrt(2),
        #                           **conv_kwargs))
        # print("h9", h9.shape)
        #
        # h10 = activ(conv_transpose(h9, 'ct10', nf=32, rf=3, stride=1, pad='SAME', init_scale=np.sqrt(2),
        #                           **conv_kwargs))
        # print("h10", h10.shape)
        # return h10

    # return network_fn
    # return create_conv_net
    if stack_attack_network == 0:
        return generator0
    elif stack_attack_network == -1:
        return generator1
    elif stack_attack_network == -2:
        return generator2
    elif stack_attack_network == -3:
        return generator3


def _normalize_clip_observation(x, clip_range=[-5.0, 5.0]):
    print("normalize_clip_observation is used")
    rms = RunningMeanStd(shape=x.shape[1:])
    norm_x = tf.clip_by_value((x - rms.mean) / rms.std, min(clip_range), max(clip_range))
    return norm_x, rms


def get_network_builder(name):
    """
    If you want to register your own network outside models.py, you just need:

    Usage Example:
    -------------
    from baselines.common.models import register
    @register("your_network_name")
    def your_network_define(**net_kwargs):
        ...
        return network_fn

    """
    if callable(name):
        return name
    elif name in mapping:
        return mapping[name]
    else:
        raise ValueError('Unknown network type: {}'.format(name))
