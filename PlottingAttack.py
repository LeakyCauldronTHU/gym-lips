import matplotlib.pyplot as plt
import numpy as np
import math
import os

import matplotlib
SMALL_SIZE = 6
matplotlib.rc('font', size=SMALL_SIZE)
matplotlib.rc('axes', titlesize=SMALL_SIZE)


def str_process(name):
    name = name.split('=')
    name_ret = ''
    # print(name)
    for i in range(len(name)):
        name_ret += name[i][0:3]
        name_ret += '_'
    name_ret = name_ret[:-1]
    return name_ret


def plotting(files_dir, name=None):
    print("stream check I")

    # assert 'att=-6' in files_dir or 'att=-7' in files_dir or 'att=-8' in files_dir

    total_timesteps, eprewmean, eval_eprewmean, eval_eprewmean_attacked, policy_entropy, \
    lipschitz_loss, policy_loss_pi, value_loss, nupdates = [], [], [], [], [], [], [], [], []

    # print(files_dir)

    logs = open(files_dir, encoding="utf8")
    line = logs.readline()

    values = []
    titles = []

    # name = 'test'
    # name_for_title = 'test'

    # print("name for title", name_for_title)

    while line:
        if 'log_path' in line and 'alg' not in line:
            tmp = line.split(' ')[-1]
            log = tmp.split('/')[-1]
        if 'total_timesteps' in line:
            a = line.split('|')
            total_timesteps.append(int(a[2]))

        if 'nupdates' in line:
            a = line.split('|')
            nupdates.append(int(a[2]))

        if 'eprewmean' in line and 'eval' not in line:
            if 'eprewmean' not in titles:
                titles.append('eprewmean')
            a = line.split('|')
            eprewmean.append(float(a[2]))

        if 'eval_eprewmean' in line and 'attacked' not in line:
            if 'eval_eprewmean' not in titles:
                titles.append('eval_eprewmean')
            a = line.split('|')
            eval_eprewmean.append(float(a[2]))

        if 'eval_eprewmean_attacked' in line:
            if 'eval_eprewmean_attacked' not in titles:
                titles.append('eval_eprewmean_attacked')
            a = line.split('|')
            eval_eprewmean_attacked.append(float(a[2]))

        if 'policy_entropy' in line and 'unattacked' not in line:
            if 'policy_entropy' not in titles:
                titles.append('policy_entropy')
            a = line.split('|')
            policy_entropy.append(float(a[2]))

        if 'lipschitz_loss' in line:
            if 'lipschitz_loss' not in titles:
                titles.append('lipschitz_loss')
            a = line.split('|')
            lipschitz_loss.append(float(a[2]))

        if 'policy_loss_pi' in line:
            if 'policy_loss_pi' not in titles:
                titles.append('policy_loss_pi')
            a = line.split('|')
            policy_loss_pi.append(float(a[2]))

        line = logs.readline()
    # print(total_timesteps)
    total_timesteps = total_timesteps[-1]
    nupdates = nupdates[-1]
    print("total_timesteps and nupdates", total_timesteps, nupdates)

    print("stream check II")

    for t in titles:
        print("titles", t)
        values.append(eval(t))

    num_fig = len(titles)
    neglect_list = ['value_loss', 'lipschitz_loss', 'policy_loss_pi', 'policy_entropy', 'nupdates']
    plt.figure()
    # plt.subplots_adjust(hspace=0.5, wspace=0.4)

    position = 0
    print("stream check III")
    for i in range(len(titles)):
        # if titles[i] in ['policy_entropy']:
        #     plt.subplot(1,2,1)
        #     plt.plot()
        #     index = total_timesteps // len(values[i]) * np.arange(len(values[i]))
        #     plt.plot(index, values[i])
        if titles[i] in neglect_list:
            print("neglected titles", titles[i])
            pass
        else:
            print("position", position, titles[i])
            print("value check", len(values[i]))
            # plt.subplot(1, 2, position + 1)
            index = total_timesteps // len(values[i]) * np.arange(len(values[i]))
            # print(values[i])
            plt.subplot(1,3, position + 1)
            print("index check", len(index))
            plt.plot(index, values[i], linewidth=2.0, label=titles[i])
            plt.xlabel('training steps')
            plt.grid()
            plt.title(titles[i])
            position += 1
    # plt.legend()
    # plt.show()

    plt.suptitle(name, ha='center', wrap=True)
    # plt.legend()
    plt.show()

    # exist = os.listdir('d:/figures_max_min')
    # if name + '.png' in exist:
    #     print("this figure already exist in this path, so we did not save it")
    # else:
    #     plt.savefig('d:/figures_max_min_new/' + name, dpi=300)
    # plt.close()
    stats = dict(zip(titles, values))
    print("stream check IV")

    # try:
    #     plt.figure()
    #     plt.subplot(2, 3, 1)
    #     index = total_timesteps // len(stats['policy_entropy'][1::2]) * np.arange(len(stats['policy_entropy'][1::2]))
    #     plt.plot(index, stats['policy_entropy'][1::2], label='attacked', linewidth=0.5)
    #     index = total_timesteps // len(stats['policy_entropy_unattacked']) * np.arange(
    #         len(stats['policy_entropy_unattacked']))
    #     plt.plot(index, stats['policy_entropy_unattacked'], label='unattacked', linewidth=0.5)
    #     plt.xlabel('training steps')
    #     plt.title('policy_entropy unattacked_VS_attacked')
    #     plt.legend()
    #     plt.grid()
    #     # plt.show()
    #     plt.subplot(2, 3, 2)
    #     index = total_timesteps // len(stats['cross_entropy_loss_init']) * np.arange(
    #         len(stats['cross_entropy_loss_init']))
    #     plt.plot(index, stats['cross_entropy_loss_init'], label='initial', linewidth=0.5)
    #     plt.plot(index, stats['cross_entropy_loss_term'], label='terminal', linewidth=0.5)
    #     plt.xlabel('training steps')
    #     plt.title("cross_entropy_loss initial_VS_termimal")
    #     plt.legend()
    #     plt.grid()
    #
    #     exist = os.listdir('d:/figures_max_min')
    #     # print("exist", exist, 'd:/figures/' + name + '_statistics' + '.png')
    #     if name + '.png' in exist:
    #         print("this figure already exist in this path, so we did not save it")
    #     else:
    #         plt.savefig('d:/figures_max_min/' + name, dpi=300)
    #     plt.show()
    #     plt.close()
    # except:
    #     pass


def get_files(path, train_type):
    files = os.listdir(path)
    final_files = []
    for file in files:
        if train_type in file:
            try:
                # subfile = os.listdir(os.path.join(path, file, 'stdout'))
                # final_files.append([os.path.join(path, file, 'stdout', subfile[-1], 'stdout.txt'), file])
                final_files.append([os.path.join(path, file, 'log.txt'), file])
            except:
                pass

    return final_files


def get_files_log(path, train_type):
    files = os.listdir(path)
    final_files = []
    for file in files:
        if train_type in file:
            final_files.append([os.path.join(path, file, 'log.txt'), file.replace('.', '_')])

    return final_files


if __name__ == "__main__":
    file_dir = 'd:/log.txt'
    # file_dir = 'd:/stdout.txt'
    path = 'c:/d/gym_lips_logs'
    # files = get_files_log(path, 'dynamic')
    files = get_files(path, 'attack')
    # print(files)
    # exit(0)
    for f in files:
        print("file", f)
        try:
            plotting(f[0], f[1])
        except:
            pass
        # exit(0)
